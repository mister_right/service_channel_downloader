import openpyxl
import os
import shutil
import time

from dotenv import load_dotenv
from openpyxl.utils import get_column_letter
from selenium import webdriver
from selenium.webdriver.common.by import By

load_dotenv()
DOWNLOAD_PATH = os.getenv('DOWNLOAD_PATH')
TARGET_PATH = os.getenv('TARGET_PATH')
USER_ID = os.getenv('USER_ID')
PASSWORD = os.getenv('PASSWORD')
SERVICE_CHANNEL_URL = os.getenv('SERVICE_CHANNEL_URL')


def get_downloaded_files():
    files = [i.split('/')[-1] for i in os.listdir(DOWNLOAD_PATH)]
    return set(files), len(files)


def download_file():
    downloaded_files, n_of_downloaded = get_downloaded_files()

    options = webdriver.ChromeOptions()
    options.add_argument('--incognito')
    options.add_argument('--start-maximized')
    options.add_argument('--disable-blink-features=AutomationControlled')
    options.add_experimental_option('prefs', {'download.default_directory': DOWNLOAD_PATH})
    driver = webdriver.Chrome(options=options)
    driver.get(SERVICE_CHANNEL_URL)
    driver.find_element(By.ID, 'UserName').send_keys(USER_ID)
    time.sleep(1)
    driver.find_element(By.ID, 'Password').send_keys(PASSWORD)
    time.sleep(1)
    driver.find_element(By.ID, 'LoginBtn').click()
    print('logged in')
    time.sleep(2)

    while True:
        try:
            dropdown = driver.find_element(By.CLASS_NAME, 'filter-dropdown-label')
            dropdown.click()
            break
        except Exception:
            print('loading')
            time.sleep(2)

    time.sleep(3)
    driver.find_element(By.XPATH, '//*[@id="filter-by-section"]/div[1]/div/ul/li/span/a[2]').click()
    time.sleep(1)
    print('filter chosen')

    was_block = False
    while True:
        loading = driver.find_element(By.ID, 'loading').value_of_css_property('display')
        print('loading')
        time.sleep(.5)
        if loading == 'block':
            was_block = True
            continue
        if was_block is True and loading == 'none':
            break
    
    time.sleep(1)
    driver.find_element(By.XPATH, '//*[@id="list-header-controls"]/div[2]/button').click()
    time.sleep(1)
    driver.find_element(By.XPATH, '//*[@id="list-header-controls"]/div[2]/ul/li[1]').click()
    time.sleep(1)

    while True:
        try:
            driver.find_element(By.XPATH, '//*[@id="downloadExcelReportModal"]/div/div/div[3]/a[2]').click()
            break
        except Exception:
            time.sleep(1)

    while True:
        _, new_n = get_downloaded_files()
        print(new_n, n_of_downloaded)
        if new_n != n_of_downloaded:
            break
        print('processing')
        time.sleep(2)

    while True:
        current_files, f = get_downloaded_files()
        tmp = current_files - downloaded_files
        new_file = next(iter(tmp))
        if new_file.endswith('.xlsx') is True:
            break
        print('downloading')
        time.sleep(2)

    print(f'downloaded file {new_file}')
    driver.close()
    return new_file


def fix_file(filepath):
    print(f'fixing {filepath}')
    workbook = openpyxl.load_workbook(filepath)
    worksheet = workbook.active
    cols_width = []
    for col in worksheet.iter_cols(1, worksheet.max_column):
        max_len = 0
        for i in range(worksheet.max_row):
            max_len = max(max_len, len(str(col[i].value)))
        cols_width.append(max_len)
    for i in range(worksheet.max_column):
        letter = get_column_letter(i + 1)
        worksheet.column_dimensions[letter].width = cols_width[i] + 5
    workbook.save(filepath)
    print('file fixed')
        

def main():
    print('start')
    new_file = download_file()
    downloaded_file_path = os.path.join(DOWNLOAD_PATH, new_file)
    fix_file(downloaded_file_path)
    shutil.move(downloaded_file_path, os.path.join(TARGET_PATH, new_file))
    print('end')


if __name__ == '__main__':
    main()
